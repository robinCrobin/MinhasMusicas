//
//  MyCell.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

class MyCell: UITableViewCell {

    @IBOutlet weak var img_capa: UIImageView!
    @IBOutlet weak var txt_nome: UILabel!
    @IBOutlet weak var txt_album: UILabel!
    @IBOutlet weak var txt_cantor: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
