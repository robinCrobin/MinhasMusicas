//
//  DetalheMusicaViewController.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

class DetalheMusicaViewController: UIViewController {
    
    var nome_imagem: String = ""
    var nome_musica: String = ""
    var nome_album: String = ""
    var nome_cantor: String = ""

    @IBOutlet weak var img_capa: UIImageView!
    @IBOutlet weak var txt_album: UILabel!
    @IBOutlet weak var txt_cantor: UILabel!
    @IBOutlet weak var txt_musica: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.img_capa.image = UIImage(named: self.nome_imagem)
        self.txt_musica.text = self.nome_musica
        self.txt_album.text = self.nome_album
        self.txt_cantor.text = self.nome_cantor
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
