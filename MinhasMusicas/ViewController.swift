//
//  ViewController.swift
//  MinhasMusicas
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

struct Musica {
    let nome_musica: String
    let nome_album: String
    let nome_cantor: String
    let nome_imagem_pequena: String
    let nome_imagem_grande: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var lista_musicas:[Musica] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.lista_musicas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath) as! MyCell
        let musica = self.lista_musicas[indexPath.row]
        
        cell.txt_nome.text = musica.nome_musica
        cell.txt_album.text = musica.nome_album
        cell.txt_cantor.text = musica.nome_cantor
        cell.img_capa.image = UIImage(named: musica.nome_imagem_pequena)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhes_view_controller = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.lista_musicas[indice]
        
        detalhes_view_controller.nome_imagem = musica.nome_imagem_grande
        detalhes_view_controller.nome_musica = musica.nome_musica
        detalhes_view_controller.nome_album = musica.nome_album
        detalhes_view_controller.nome_cantor = musica.nome_cantor
    }

    @IBOutlet weak var tableView_musicas: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView_musicas.dataSource = self
        self.tableView_musicas.delegate = self
        
        self.lista_musicas.append(Musica(nome_musica: "Pontos Cardeais", nome_album: "Álbum vivo!", nome_cantor: "Alceu Valença", nome_imagem_pequena: "capa_alceu_pequeno", nome_imagem_grande: "capa_alceu_grande"))
        
        self.lista_musicas.append(Musica(nome_musica: "Menor Abandonado", nome_album: "Álbum Patotade Cosme", nome_cantor: "Zeca Pagodinho", nome_imagem_pequena: "capa_zeca_pequeno", nome_imagem_grande: "capa_zeca_grande"))
        
        self.lista_musicas.append(Musica(nome_musica: "Tiro ao Álvaro", nome_album: "Álbum Adoniran Barbosa e Convidados", nome_cantor: "Adoniran Barbosa", nome_imagem_pequena: "capa_adoniran_pequeno", nome_imagem_grande: "capa_adhoniran_grande"))
    }


}

